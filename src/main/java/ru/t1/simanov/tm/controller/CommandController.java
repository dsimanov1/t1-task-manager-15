package ru.t1.simanov.tm.controller;

import ru.t1.simanov.tm.api.controller.ICommandController;
import ru.t1.simanov.tm.api.service.ICommandService;
import ru.t1.simanov.tm.model.Command;
import ru.t1.simanov.tm.util.FormatUtil;

public final class CommandController implements ICommandController {

    public final ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showInfo() {
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryFormat = FormatUtil.formatBytes(freeMemory);
        System.out.println("Free memory: " + freeMemoryFormat);
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = FormatUtil.formatBytes(maxMemory);
        final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;
        final String maxMemoryFormat = maxMemoryCheck ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory: " + maxMemoryFormat);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryFormat = FormatUtil.formatBytes(totalMemory);
        System.out.println("Total memory: " + totalMemoryFormat);
        final long usageMemory = totalMemory - freeMemory;
        final String usageMemoryFormat = FormatUtil.formatBytes(usageMemory);
        System.out.println("Usage memory: " + usageMemoryFormat);
    }

    @Override
    public void showWelcome() {
        System.out.println("** WELCOME TO TASK-MANAGER**");
    }

    @Override
    public void showEnter() {
        System.out.println("ENTER COMMAND:");
    }

    @Override
    public void showErrorArgument() {
        System.out.println("Error! This argument not supported...");
    }

    @Override
    public void showArguments() {
        System.out.println("[ARGUMENTS]");

        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) {
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    public void showCommands() {
        System.out.println("[COMMANDS]");

        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) {
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

    @Override
    public void showVersion() {
        System.out.println("1.15.0");
    }

    @Override
    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Dima Simanov");
        System.out.println("E-mail: simanov.dima@gmail.com");
    }

    @Override
    public void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) System.out.println(command);
    }

    @Override
    public void showErrorCommand() {
        System.out.println("Error! This command not supported...");
    }

}
